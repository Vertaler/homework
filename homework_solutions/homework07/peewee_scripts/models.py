from peewee import *
from utils import read_config, recreate_tables

config = read_config('./config')
db = PostgresqlDatabase(**config)


class Customer(Model):
    cust_id = PrimaryKeyField()
    first_nm = CharField(max_length=100)
    last_nm = CharField(max_length=100)

    class Meta:
        database = db
        db_table = 'customers'


class Order(Model):
    order_id = PrimaryKeyField()
    status = CharField(max_length=20, default='status')
    customer = ForeignKeyField(model=Customer, field='cust_id')
    order_dttm = DateTimeField(default='1999-01-08')

    class Meta:
        db_table = 'orders'
        database = db


class Good(Model):
    good_id = PrimaryKeyField()
    name = CharField(max_length=100)
    vendor = CharField(max_length=100)
    description = CharField(max_length=300)

    class Meta:
        db_table = 'goods'
        database = db

class OrderItem(Model):
    good = ForeignKeyField(model=Good, field='good_id')
    order = ForeignKeyField(model=Order, field='order_id')
    quantity = IntegerField()

    class Meta:
        db_table = 'order_items'
        database = db

def create_tables():
    recreate_tables(db, Customer, Order, Good, OrderItem)