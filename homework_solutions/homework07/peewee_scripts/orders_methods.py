from models import OrderItem
import csv

def add_good_to_order(order, good):
    order_item = OrderItem()
    order_item.order = order
    order_item.good = good
    order_item.quantity = 1
    order_item.save()



def remove_good_from_order(order, good):
    OrderItem.delete().where(OrderItem.order == order and OrderItem.good == good).execute()


def update_quantity(order_item, new_quantity):
    order_item.quantity = new_quantity
    order_item.save()


def order_item_to_dict(order_item):
       cust = order_item.order.customer
       result = {}
       result['order_id'] = order_item.order.order_id
       result['name'] = order_item.good.name
       result['vendor'] = order_item.good.vendor
       result['quantity'] = order_item.quantity
       result['cust_name'] = '%s %s' % (cust.first_nm, cust.last_nm)
       return result

def save_order_items_to_csv():
    with open('order_items.csv', 'w') as csvfile:
        fieldnames = ['order_id', 'name', 'vendor', 'quantity', 'cust_name']
        writer = csv.DictWriter(csvfile, fieldnames)
        writer.writeheader()

        for order_item in OrderItem.select():
            writer.writerow(order_item_to_dict(order_item))

