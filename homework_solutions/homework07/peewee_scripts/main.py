import models
import orders_methods
from utils import fill_database

def test_orders_methods():
    cust = models.Customer()
    cust.first_nm = 'test'
    cust.last_nm = 'test'
    cust.save()

    good = models.Good()
    good.vendor = 'test'
    good.name = 'test'
    good.description ='test'
    good.save()

    order = models.Order()
    order.customer = cust
    order.save()

    orders_methods.add_good_to_order(order, good)
    ord_it = models.OrderItem.get(models.OrderItem.good == good and models.OrderItem.order == order)
    print 'Succesfully added good to order'

    orders_methods.update_quantity(ord_it, 10)
    if ord_it.quantity == 10:
        print 'Succesfully updated good quantity'

    orders_methods.remove_good_from_order(order, good)
    if not len(models.OrderItem.select().where(models.OrderItem.good == good and models.OrderItem.order == order)):
        print 'Succesfully removed good from order'

if __name__ == '__main__':
    models.create_tables()
    fill_database()
    test_orders_methods()
    orders_methods.save_order_items_to_csv()