import configparser
import os
import random


def fill_database():
    #Protect from circular dependencies
    from models import Customer, OrderItem, Order, Good

    Customer.delete().execute()
    OrderItem.delete().execute()
    Order.delete().execute()
    Good.delete().execute()

    for i in range(5):
        Customer.insert(
            {'first_nm':'first %d' % i, 'last_nm' : 'last %d' %i}
        ).execute()
        Good.insert(
            {'vendor': 'vendor %d' % i, 'name': 'name %d' % i, 'description' : 'desc %d' % i}
        ).execute()

    for customer in Customer.select():
        order = Order()
        order.customer = customer
        order.save()

    all_goods =  Good.select()
    all_orders = Order.select()

    for i in range(15):
        order_item = OrderItem()
        order_item.good = random.choice(all_goods)
        order_item.order = random.choice(all_orders)
        order_item.quantity = random.randint(1,5)
        order_item.save()

def recreate_tables(db, *models):
    db.drop_tables(models)
    db.create_tables(models)

def read_config(conf_path):
    confparser = configparser.ConfigParser()
    confparser.read(conf_path)
    config = dict(confparser["postgresql"])
    config['password'] = os.environ[config['password_envvar']]
    del config['password_envvar']
    return config
