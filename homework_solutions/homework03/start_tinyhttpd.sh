#!/bin/sh

#TINYHTTPD variable contains path to httpd server
IS_RUN=$(ps aux | grep $TINYHTTPD | grep -v grep | wc -l)
if [[ $IS_RUN = 0 ]]
then
        cd $(dirname $TINYHTTPD)
        $TINYHTTPD &
        echo "$(date) httpd was not running, starting" >> /root/httpd_start.log
fi


