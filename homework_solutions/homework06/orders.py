import csv

class Orders:
    def __init__(self, cur, conn):
        self.cur = cur
        self.con = conn

    def add_good_to_order(self, order_id, good_id):
        sql = "INSERT INTO order_items (order_id, good_id, quantity) VALUES(%s,%s,1);"
        self.cur.execute(sql, [order_id, good_id])
        self.con.commit()

    def remove_good_from_order(self, order_id, good_id):
        sql ='DELETE FROM order_items WHERE order_id = %s and good_id = %s;'
        self.cur.execute(sql, [order_id, good_id])
        self.con.commit()

    def update_quantity(self, order_item_id, new_quantity):
        sql = 'UPDATE order_items SET quantity=%s WHERE order_item_id=%s;'
        self.cur.execute(sql, [new_quantity, order_item_id])
        self.con.commit()

    def save_to_csv(self):
        sql = '''Select orders.order_id, name, vendor, quantity , first_nm || ' ' || last_nm AS cust_name  FROM order_items 
                 INNER JOIN goods ON 
                 order_items.good_id = goods.good_id
                 INNER JOIN orders ON
                 order_items.order_id = orders.order_id
                 INNER JOIN customers ON
                 orders.cust_id = customers.cust_id;
        '''

        self.cur.execute(sql)
        data =  self.cur.fetchall()

        with open('order_items.csv','w') as csvfile:
            fieldnames = ['order_id', 'name', 'vendor', 'quantity', 'cust_name']
            writer = csv.writer(csvfile)
            writer.writerow(fieldnames)
            writer.writerows(data)
