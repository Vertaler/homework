from random import randint

CUSTOMERS_COUNT = 5
GOODS_COUNT = 5
ORDERS_COUNT = 10
ORDER_ITEMS_COUNT = 15

MAX_QUANTITY = 5

def _clear_tables(cur, conn):
    clear_template = 'DELETE FROM {}'
    tables = [
        'customers',
        'orders',
        'goods',
        'order_items'
    ]

    for table in tables:
        cur.execute(clear_template.format(table))
    conn.commit()

def _reset_primary_keys(cur, conn):
    reset_pk_temlate = 'ALTER SEQUENCE {} RESTART WITH 1'
    seqs = [
        'customers_cust_id_seq',
        'orders_order_id_seq',
        'goods_good_id_seq',
        'order_items_order_item_id_seq'
    ]

    for seq in seqs:
        cur.execute(reset_pk_temlate.format(seq))
    conn.commit()

def fill_database(cur, conn):
    _clear_tables(cur, conn)
    _reset_primary_keys(cur, conn)

    fill_customers_sql = "INSERT INTO customers (first_nm, last_nm) VALUES(%s, %s);"
    fill_orders_sql = "INSERT INTO orders (cust_id, order_dttm, status) VALUES(%s, %s, %s);"
    fill_goods_sql = "INSERT INTO goods (vendor, name, description) VALUES(%s, %s, %s);"
    fill_order_items_sql = "INSERT INTO order_items (order_id, good_id, quantity) VALUES (%s,%s,%s);"

    customers_vars = ['First{0} Last{0}'.format(i).split(' ') for i in range(CUSTOMERS_COUNT)]
    goods_vars = ['Vendor{0} Good{0} Desc{0}'.format(i).split(' ') for i in range(GOODS_COUNT) ]
    orders_vars = [[randint(1,CUSTOMERS_COUNT),'1999-01-08', 'Status'] for _ in range(ORDERS_COUNT)]
    order_items_vars = [
        map(lambda x: randint(1,x), [ORDERS_COUNT, GOODS_COUNT, MAX_QUANTITY]) for _ in range(ORDER_ITEMS_COUNT)
    ]

    cur.executemany(fill_customers_sql, customers_vars)
    cur.executemany(fill_goods_sql, goods_vars)
    cur.executemany(fill_orders_sql, orders_vars)
    cur.executemany(fill_order_items_sql, order_items_vars)
    conn.commit()