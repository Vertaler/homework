import configparser
import psycopg2
import os
from orders import Orders
from fill_database import fill_database
from creates import create_tables

if __name__ == "__main__":
    confparser =  configparser.ConfigParser()
    confparser.read('config')
    config = dict( confparser["postgresql"])
    config['password'] = os.environ[config['password_envvar']]
    del config['password_envvar']

    conn = psycopg2.connect(**config)
    try:
        cur = conn.cursor()

        create_tables(cur, conn)
        print('Tables created')

        fill_database(cur, conn)
        print('Database filled out')

        orders = Orders(cur, conn)
        orders.save_to_csv()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
