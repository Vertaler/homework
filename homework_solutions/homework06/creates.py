commands = [
    'DROP TABLE IF EXISTS customers CASCADE',
    'DROP TABLE IF EXISTS orders CASCADE',
    'DROP TABLE IF EXISTS goods CASCADE',
    'DROP TABLE IF EXISTS order_items',
    '''CREATE TABLE customers (
     cust_id serial PRIMARY KEY,
     first_nm varchar(100),
     last_nm varchar(100)
     )''',

    '''CREATE TABLE orders (
     order_id serial PRIMARY KEY, 
     cust_id integer, 
     order_dttm timestamp, 
     status varchar(20),
     FOREIGN KEY (cust_id)
     REFERENCES customers (cust_id)
     ON UPDATE CASCADE ON DELETE CASCADE
     )''',

    '''CREATE TABLE goods (
     good_id serial PRIMARY KEY, 
     vendor varchar(100),
     name varchar(100),
     description varchar(300)
     )''',
    '''CREATE TABLE order_items (
     order_item_id serial,
     order_id integer, 
     good_id integer, 
     quantity integer,
     FOREIGN KEY (good_id)
     REFERENCES goods (good_id)
     ON UPDATE CASCADE ON DELETE CASCADE,
     FOREIGN KEY (order_id)
     REFERENCES orders (order_id)
     ON UPDATE CASCADE ON DELETE CASCADE
     )''',
]

def create_tables(cur, conn):
    for command in commands:
        cur.execute(command)
    conn.commit()

